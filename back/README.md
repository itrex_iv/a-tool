
## Production run

```bash
$ docker-compose up
```

## Documentation generation

```bash
npx compodoc -p tsconfig.json -s
```