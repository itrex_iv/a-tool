import { ScheduleModule } from 'nest-schedule';
import { Module, HttpModule } from '@nestjs/common';
import { AnnotatorsModule } from './modules/annotators/annotators.module';
import { CoreModule } from './core/core.module';
import { FramesModule } from './modules/frames/frames.module';
import { AnnotationsModule } from './modules/annotations/annotations.module';
import { JobsModule } from './modules/jobs/jobs.module';
import { ImagesModule } from './modules/images/images.module';
import { sharedModules } from './shared/shared.module';

@Module({
  imports: [...sharedModules, CoreModule, AnnotatorsModule, FramesModule, AnnotationsModule, JobsModule, ImagesModule, ScheduleModule.register()],
  controllers: [],
  providers: [],
})
export class AppModule {}
