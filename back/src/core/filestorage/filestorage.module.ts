import { Module } from '@nestjs/common';
import { FilestorageService } from './filestorage.service';

@Module({
  providers: [FilestorageService],
  exports: [FilestorageService],
})
export class FilestorageModule {}
