import * as fs from 'fs-extra';
import { Injectable } from '@nestjs/common';
import { ENV_VARIABLE_KEY } from '../../shared/config/constants';
import { ConfigService } from '../../shared/config/services';

@Injectable()
export class FilestorageService {
  public readonly rootDir: string;

  constructor(public readonly configService: ConfigService) {
    this.rootDir = this.configService.get(ENV_VARIABLE_KEY.FS_ROOT_DIR);
  }

  async getDirContents(searchDirectory: string): Promise<string[]> {
    return fs.readdir(searchDirectory);
  }

  async checkFileDirExist(searchDirectory: string): Promise<boolean> {
    return fs.pathExists(searchDirectory);
  }

  async getFileContent<T>(filePath: string): Promise<T> {
    return (await fs.readJson(filePath)) as T;
  }

  async writeFile<T>(filePath: string, content: T): Promise<void> {
    return fs.writeJson(filePath, content);
  }

  createReadStream(sourceFilePath: string): any {
    return fs.createReadStream(sourceFilePath);
  }
}
