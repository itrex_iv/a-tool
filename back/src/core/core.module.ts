import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { FilestorageModule } from './filestorage/filestorage.module';
import { AuthModule } from './auth/auth.module';
import { ScheduleService } from './scheduleJobs/schedule.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [CqrsModule, DatabaseModule, AuthModule, FilestorageModule],
  providers: [ScheduleService],
  exports: [DatabaseModule, AuthModule, FilestorageModule],
})
export class CoreModule {}
