import * as mongoose from 'mongoose';
import { ConfigService } from '../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../shared/config/constants';

mongoose.set('useFindAndModify', false);

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (configService: ConfigService): Promise<typeof mongoose> =>
      mongoose.connect(configService.get(ENV_VARIABLE_KEY.DB_HOST), {
        useNewUrlParser: true,
      }),
    inject: [ConfigService],
  },
];
