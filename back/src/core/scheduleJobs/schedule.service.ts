import { Injectable } from '@nestjs/common';
import { Cron, NestSchedule } from 'nest-schedule';
import { CommandBus } from '@nestjs/cqrs';
import { GetNewJobsCommand } from '../../modules/jobs/commands/impl';
import { AppLogger } from '../../shared/logger/logger.service';

@Injectable()
export class ScheduleService extends NestSchedule {
  constructor(public readonly commandBus: CommandBus, public readonly appLogger: AppLogger) {
    super();
  }

  @Cron('*/10 * * * *', {
    waiting: true,
    immediate: true,
    maxRetry: 2,
  })
  async cronJob() {
    this.appLogger.log('Executing Cron Job');
    await this.commandBus.execute(new GetNewJobsCommand());
  }
}
