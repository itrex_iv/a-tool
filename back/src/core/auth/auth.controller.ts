import { Controller, UseGuards, Post, UseInterceptors, ClassSerializerInterceptor, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './services/auth.service';
import { ApiUseTags, ApiResponse, ApiOkResponse } from '@nestjs/swagger';
import { LoginSerializer } from './serializers/login.serializer';
import { InternalServerErrorOpenAPIResponse } from '../../shared/openapi/internal-server-error';
import { LoginUnauthorizedOpenAPIResponse } from './openapi/user-login-unauthorized-error';
import { BadRequestErrorOpenAPIResponse } from '../../shared/openapi/bad-request-error';

@ApiUseTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOkResponse({ type: LoginSerializer })
  @ApiResponse(LoginUnauthorizedOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @UseGuards(AuthGuard('local'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('login')
  async login(@Request() req) {
    return new LoginSerializer(await this.authService.login(req.user));
  }
}
