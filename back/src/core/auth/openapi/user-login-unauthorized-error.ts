import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  UsernameIsNotFound = 'Username is not found!',
  PasswordIsInvalid = 'Password is invalid!',
}

export class LoginUnauthorizedOpenAPIResponseType {
  @ApiModelProperty({ example: 'Unauthorized' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const LoginUnauthorizedOpenAPIResponse = {
  status: HttpStatus.UNAUTHORIZED,
  type: LoginUnauthorizedOpenAPIResponseType,
};
