import * as bcrypt from 'bcrypt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { QueryBus } from '@nestjs/cqrs';
import { GetAnnotatorQuery } from '../../../modules/annotators/queries/impl';
import { Annotator } from '../../../modules/annotators/interfaces';
import { message } from '../openapi/user-login-unauthorized-error';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService, private readonly queryBus: QueryBus) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.queryBus.execute<GetAnnotatorQuery, Annotator>(new GetAnnotatorQuery(username));
    if (!user) throw new UnauthorizedException(message.UsernameIsNotFound);
    if (!bcrypt.compareSync(pass, user.password)) throw new UnauthorizedException(message.PasswordIsInvalid);

    const { password, ...result } = user;
    return result;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user._id, roles: user.roles };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
