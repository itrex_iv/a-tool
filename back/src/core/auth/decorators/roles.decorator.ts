import { SetMetadata } from '@nestjs/common';

/* tslint:disable */
export const Roles = (...roles: string[]) => SetMetadata('roles', roles);
