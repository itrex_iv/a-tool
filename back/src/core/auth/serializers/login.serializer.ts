import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class LoginSerializer {
  @ApiModelProperty()
  @Expose()
  access_token: string;

  constructor(partial: Partial<LoginSerializer>) {
    Object.assign(this, partial);
  }
}
