import { Frame } from '../../frames/interfaces';

export interface Annotation {
  session: number;
  job_id: string;
  annotation_id: number;
  user_id: number;
  annotation_name: string;
  created_at: string;
  updated_at: string;
  multi_view: Timestamp[];
}

export interface Timestamp {
  timestamp: number;
  frames: Frame[];
}
