import { Module } from '@nestjs/common';
import { FilestorageModule } from '../../core/filestorage/filestorage.module';
import { JobsModule } from '../jobs/jobs.module';
import { commandHandlers } from './commands/handlers/index';
import { CqrsModule } from '@nestjs/cqrs';
import { AnnotationsRepository } from './repository/annotations.repository';
import { FramesModule } from '../frames/frames.module';
import { eventHandlers } from './events/handlers';
import { AnnotationsSagas } from './sagas/annotations.sagas';

@Module({
  imports: [CqrsModule, FilestorageModule, JobsModule, FramesModule],
  providers: [AnnotationsRepository, ...commandHandlers, ...eventHandlers, AnnotationsSagas],
})
export class AnnotationsModule {}
