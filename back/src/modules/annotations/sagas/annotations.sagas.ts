import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { SendAnnotationCommand } from '../commands/impl';
import { AnnotationSessionFinishingEvent } from '../../annotators/events/impl/annotation-session-finishing.event';

@Injectable()
export class AnnotationsSagas {
  @Saga()
  sessionFinishing = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(AnnotationSessionFinishingEvent),
      delay(1000),
      map(event => {
        return new SendAnnotationCommand(event.annotator_id, event.job_id);
      }),
    );
  };
}
