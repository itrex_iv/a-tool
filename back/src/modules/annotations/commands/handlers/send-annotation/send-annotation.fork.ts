import * as fs from 'fs-extra';
import axios from 'axios';
import { Timestamp, Annotation } from '../../../interfaces';
import { Frame } from '../../../../frames/interfaces';
import * as uuid from 'uuid';
import * as moment from 'moment';
import * as _ from 'lodash';

process.on('message', async ({ annotator_id, job_id, job, jobsDir, baseURL }) => {
  try {
    const camerasIds = await getSessionsCamerasIds(jobsDir, annotator_id, job_id);
    const indexes = await getAllCameraIndexes(jobsDir, annotator_id, job_id, camerasIds);
    const annotationPromise = indexes.map(
      async (frameIndex): Promise<Timestamp> => {
        const { timestamp } = await getFrame(jobsDir, annotator_id, job_id, Number(camerasIds[0]), Number(frameIndex));
        const frames = await Promise.all(
          camerasIds.map(
            async (camera_id): Promise<Frame> => {
              const { index, frameTime, timestamp, ...frame } = await getFrame(jobsDir, annotator_id, job_id, Number(camera_id), Number(frameIndex));
              return frame;
            },
          ),
        );
        return {
          timestamp,
          frames: _.filter(frames, i => !_.isEmpty(i)),
        };
      },
    );
    const multi_view = await Promise.all(annotationPromise);
    const annotation: Annotation = {
      multi_view,
      session: job.session,
      job_id: job.job_id,
      annotation_id: uuid(),
      user_id: annotator_id,
      annotation_name: `${job.job_id}:${annotator_id}:${moment().unix()}`,
      created_at: moment().format(),
      updated_at: moment().format(),
    };
    await saveAnnotation(baseURL, annotation);
    await deleteSession(jobsDir, annotator_id, job_id);
    process.send({ error: false });
  } catch (e) {
    process.send({ error: true, msg: e });
  }
});

async function getSessionsCamerasIds(jobsDir: string, annotator_id: string, job_id: string): Promise<string[]> {
  const pathToSession = `${jobsDir}/${job_id}/sessions/${annotator_id}/`;
  return (await fs.readdir(pathToSession)).filter(dir => dir !== '.DS_Store');
}

async function getAllCameraIndexes(jobsDir: string, annotator_id: string, job_id: string, camerasIds: string[]): Promise<number[]> {
  const indexes = await Promise.all(camerasIds.map(async cameraId => getCameraIndexes(jobsDir, annotator_id, job_id, cameraId)));
  const maxIndex = Math.max.apply(Math, _.flatten(indexes));
  return _.range(maxIndex + 1);
}

async function getCameraIndexes(jobsDir: string, annotator_id: string, job_id: string, camera_id: string): Promise<string[]> {
  const pathToCamera = `${jobsDir}/${job_id}/sessions/${annotator_id}/${camera_id}/`;
  return (await fs.readdir(pathToCamera)).filter(dir => dir !== '.DS_Store');
}

async function getFrame(jobsDir: string, annotator_id: string, job_id: string, camera_id: number, index: number): Promise<Frame> {
  const pathToFrame = `${jobsDir}/${job_id}/sessions/${annotator_id}/${camera_id}/${index}/frame.json`;
  const frameExist = await fs.exists(pathToFrame);
  return frameExist ? fs.readJson(pathToFrame) : {};
}

async function saveAnnotation(baseURL: string, annotation: Annotation): Promise<void> {
  return axios.post(`annotations`, annotation, { baseURL });
}

async function deleteSession(jobsDir: string, annotator_id: string, job_id: string): Promise<void> {
  const pathToSession = `${jobsDir}/${job_id}/sessions/${annotator_id}/`;
  return fs.removeSync(pathToSession);
}
