import { fork } from 'child_process';
import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { SendAnnotationCommand } from '../../impl';
import { JobsRepository } from '../../../../jobs/repository/jobs.repository';
import { SendAnnotationFailureEvent } from '../../../events/impl/send-annotation-failure.event';
import { SendAnnotationSuccessEvent } from '../../../events/impl/send-annotation-success.event';
import { ConfigService } from '../../../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../../../shared/config/constants';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(SendAnnotationCommand)
export class SendAnnotationHandler implements ICommandHandler<SendAnnotationCommand> {
  constructor(
    private readonly jobsRepository: JobsRepository,
    private readonly eventBus: EventBus,
    private readonly configService: ConfigService,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ annotator_id, job_id }: SendAnnotationCommand) {
    this.appLogger.command(`SendAnnotation`);
    try {
      const job = await this.jobsRepository.getJobById(job_id);
      const forked = fork(`${__dirname}/send-annotation.fork.js`);

      forked.send({
        annotator_id,
        job_id,
        job,
        jobsDir: this.jobsRepository.jobsDir,
        baseURL: this.configService.get(ENV_VARIABLE_KEY.JOB_SERVER_DOMAIN),
      });

      forked.on('message', msg => {
        if (msg.error) {
          console.log(msg);
          this.eventBus.publish(new SendAnnotationFailureEvent(annotator_id, job_id));
        } else this.eventBus.publish(new SendAnnotationSuccessEvent(annotator_id, job_id));
      });
    } catch (e) {
      console.log(e);
      this.eventBus.publish(new SendAnnotationFailureEvent(annotator_id, job_id));
    }
  }
}
