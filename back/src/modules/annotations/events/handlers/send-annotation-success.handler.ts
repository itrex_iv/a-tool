import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { SendAnnotationSuccessEvent } from '../impl/send-annotation-success.event';
import { AppLogger } from '../../../../shared/logger/logger.service';

@EventsHandler(SendAnnotationSuccessEvent)
export class SendAnnotationSuccessHandler implements IEventHandler<SendAnnotationSuccessEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: SendAnnotationSuccessEvent) {
    this.appLogger.event(`SendAnnotationSuccess`);
    return;
  }
}
