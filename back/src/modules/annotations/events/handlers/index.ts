import { SendAnnotationFailureHandler } from './send-annotation-failure.handler';
import { SendAnnotationSuccessHandler } from './send-annotation-success.handler';

export const eventHandlers = [SendAnnotationFailureHandler, SendAnnotationSuccessHandler];
