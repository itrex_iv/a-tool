import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { SendAnnotationFailureEvent } from '../impl/send-annotation-failure.event';
import { AppLogger } from '../../../../shared/logger/logger.service';

@EventsHandler(SendAnnotationFailureEvent)
export class SendAnnotationFailureHandler implements IEventHandler<SendAnnotationFailureEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: SendAnnotationFailureEvent) {
    this.appLogger.event(`SendAnnotationFailure`);
    return;
  }
}
