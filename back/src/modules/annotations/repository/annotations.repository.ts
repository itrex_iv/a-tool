import { Injectable } from '@nestjs/common';
import { FilestorageService } from '../../../core/filestorage/filestorage.service';
import { Timestamp } from '../interfaces';

@Injectable()
export class AnnotationsRepository {
  public readonly jobsDir: string;
  public readonly annotationsDir: string;
  constructor(public readonly filestorageService: FilestorageService) {
    this.annotationsDir = `${this.filestorageService.rootDir}/annotations`;
  }

  // async save(annotator_id: string, job_id: string, annotation: Timestamp[]): Promise<void> {
  //   const pathToFrame = `${this.annotationsDir}/${job_id}_${annotator_id}.json`;
  //   return this.filestorageService.writeFile<Timestamp[]>(pathToFrame, annotation);
  // }
}
