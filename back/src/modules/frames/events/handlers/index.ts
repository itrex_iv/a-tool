import { AnnotationSessionUpdatingHandler } from './annotation-session-updating.event.handler';

export const eventHandlers = [AnnotationSessionUpdatingHandler];
