import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { AnnotationSessionUpdatingEvent } from '../impl/annotation-session-finishing.event';
import { AppLogger } from '../../../../shared/logger/logger.service';

@EventsHandler(AnnotationSessionUpdatingEvent)
export class AnnotationSessionUpdatingHandler implements IEventHandler<AnnotationSessionUpdatingEvent> {
  constructor(private readonly appLogger: AppLogger) {}
  handle(event: AnnotationSessionUpdatingEvent) {
    this.appLogger.event(`AnnotationSessionUpdating`);
    return;
  }
}
