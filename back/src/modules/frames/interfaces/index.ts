export interface Frame {
  job_id: string;
  camera_id: number;
  camera_name: string;
  image_url: string;
  index?: number;
  frameTime?: number;
  timestamp?: number;
  detections: Track[];
}

export interface Track {
  track_id: number;
  kpts: Kpt[];
  updated_at?: number;
}

export interface Kpt {
  kpt_name: KptType;
  x: number;
  y: number;
  score: number;
  updated_at?: number;
}

export enum KptType {
  Nose = 'Nose',
  Neck = 'Neck',
  RShoulder = 'RShoulder',
  LShoulder = 'LShoulder',
  RElbow = 'RElbow',
  LElbow = 'LElbow',
  RWrist = 'RWrist',
  LWrist = 'LWrist',
  MidHip = 'MidHip',
  RHip = 'RHip',
  LHip = 'LHip',
  RKnee = 'RKnee',
  LKnee = 'LKnee',
  RAnkle = 'RAnkle',
  LAnkle = 'LAnkle',
  REye = 'REye',
  LEye = 'LEye',
  REar = 'REar',
  LEar = 'LEar',
  LBigToe = 'LBigToe',
  RBigToe = 'RBigToe',
  LSmallToe = 'LSmallToe',
  RSmallToe = 'RSmallToe',
  LHell = 'LHeel',
  RHell = 'RHeel',
  TopHead = 'TopHead',
}
