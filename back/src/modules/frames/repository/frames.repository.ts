import { Injectable } from '@nestjs/common';
import { Frame } from '../interfaces';
import { FilestorageService } from '../../../core/filestorage/filestorage.service';

@Injectable()
export class FramesRepository {
  public readonly jobsDir: string;
  constructor(public readonly filestorageService: FilestorageService) {
    this.jobsDir = `${this.filestorageService.rootDir}/jobs`;
  }

  async getFrame(annotator_id: string, job_id: string, camera_id: number, index: number): Promise<Frame> {
    const pathToFrame = `${this.jobsDir}/${job_id}/sessions/${annotator_id}/${camera_id}/${index}/frame.json`;
    return this.filestorageService.getFileContent<Frame>(pathToFrame);
  }

  async checkIfFrameExist(annotator_id: string, job_id: string, camera_id: number, index: number): Promise<boolean> {
    const pathToFrame = `${this.jobsDir}/${job_id}/sessions/${annotator_id}/${camera_id}/${index}/frame.json`;
    return this.filestorageService.checkFileDirExist(pathToFrame);
  }

  async update(annotator_id: string, job_id: string, camera_id: number, index: number, frame: Frame): Promise<void> {
    const pathToFrame = `${this.jobsDir}/${job_id}/sessions/${annotator_id}/${camera_id}/${index}/frame.json`;
    await this.filestorageService.writeFile<Frame>(pathToFrame, frame);
  }
}
