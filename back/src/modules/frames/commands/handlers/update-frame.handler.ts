import { CommandHandler, ICommandHandler, EventBus, QueryBus } from '@nestjs/cqrs';
import { UpdateFrameCommand } from '../impl/update-frame.command';
import { FramesRepository } from '../../repository/frames.repository';
import { NotFoundException } from '@nestjs/common';
import { AnnotationSessionUpdatingEvent } from '../../events/impl/annotation-session-finishing.event';
import { GetSessionQuery } from '../../../annotators/queries/impl';
import { Session } from '../../../annotators/interfaces';
import { message } from '../../openapi/frame-not-found';
import { AppLogger } from '../../../../shared/logger/logger.service';

@CommandHandler(UpdateFrameCommand)
export class UpdateFrameHandler implements ICommandHandler<UpdateFrameCommand> {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly framesRepository: FramesRepository,
    private readonly eventBus: EventBus,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ paramDto, bodyDto }: UpdateFrameCommand) {
    this.appLogger.command(`UpdateFrame`);
    const { annotator_id, job_id, camera_id, index } = paramDto;
    const { detections } = bodyDto;

    const frameExist = await this.framesRepository.checkIfFrameExist(annotator_id, job_id, camera_id, index);
    if (!frameExist) throw new NotFoundException(message.FrameNotFound);
    const frame = await this.framesRepository.getFrame(annotator_id, job_id, camera_id, index);
    const session: Session = await this.queryBus.execute(new GetSessionQuery(annotator_id, job_id));

    const detectionsWithTrackIdReducer = (a, detection) => a + (detection.track_id === -1 ? 0 : 1);
    const detections_with_track_id_old = frame.detections.reduce(detectionsWithTrackIdReducer, 0);
    const detections_with_track_id_new = detections.reduce(detectionsWithTrackIdReducer, 0);
    const detections_with_track_id = session.detections_with_track_id - detections_with_track_id_old + detections_with_track_id_new;

    const detections_qty_old = frame.detections.length;
    const detections_qty_new = detections.length;
    const detections_qty = session.detections_qty - detections_qty_old + detections_qty_new;

    await this.framesRepository.update(annotator_id, job_id, camera_id, index, { ...frame, detections });
    this.eventBus.publish(new AnnotationSessionUpdatingEvent(annotator_id, job_id, detections_qty, detections_with_track_id));
  }
}
