import { UpdateFrameForCameraDto, GetFramesForCameraDto } from '../../dto';

export class UpdateFrameCommand {
  constructor(public readonly paramDto: GetFramesForCameraDto, public readonly bodyDto: UpdateFrameForCameraDto) {}
}
