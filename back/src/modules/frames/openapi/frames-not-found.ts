import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  FramesNotFound = 'Frames not found!',
  AnnotatorNotFound = 'Annotator not found!',
}

export class FramesNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const FramesNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: FramesNotFoundOpenAPIResponseType,
};
