import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  FrameNotFound = 'Frame not found!',
  AnnotatorNotFound = 'Annotator not found!',
}

export class FrameNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const FrameNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: FrameNotFoundOpenAPIResponseType,
};
