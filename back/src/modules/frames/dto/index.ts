export * from './get-frames-for-all-cameras.dto';
export * from './get-frames-for-camera.dto';
export * from './update-frame-for-camera.dto';
