import { IsString, Min, IsNumber } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export class GetFramesForAllCamerasDto {
  @ApiModelProperty()
  @IsString()
  public readonly annotator_id: string;

  @ApiModelProperty()
  @IsString()
  public readonly job_id: string;

  @ApiModelProperty({ minimum: 0 })
  @IsNumber()
  @Min(0)
  @Transform(value => Number(value))
  public readonly index: number;
}
