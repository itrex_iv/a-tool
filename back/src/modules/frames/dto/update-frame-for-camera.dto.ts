/* tslint:disable:max-classes-per-file*/

import { Track, Kpt, KptType } from '../interfaces';
import { ValidateNested, IsNumber, IsEnum, IsOptional, IsArray } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class KptDto {
  @ApiModelProperty()
  @IsEnum(KptType)
  kpt_name: KptType;

  @ApiModelProperty()
  @IsNumber()
  x: number;

  @ApiModelProperty()
  @IsNumber()
  y: number;

  @ApiModelProperty()
  @IsNumber()
  score: number;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumber()
  updated_at: number;
}

export class TrackDto {
  @ApiModelProperty()
  @IsNumber()
  track_id: number;

  @ApiModelProperty({ type: [KptDto] })
  @ValidateNested({ each: true })
  @Type(() => KptDto)
  kpts: Kpt[];

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumber()
  updated_at: number;
}

export class UpdateFrameForCameraDto {
  @ApiModelProperty({ type: [TrackDto] })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => TrackDto)
  detections: Track[];
}
