import { Exclude, Expose } from 'class-transformer';
import { Track } from '../interfaces';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class FrameSerializer {
  @ApiModelProperty()
  @Exclude()
  job_id: string;

  @ApiModelProperty()
  @Expose()
  camera_id: number;

  @ApiModelProperty()
  @Expose()
  camera_name: string;

  @ApiModelProperty()
  @Expose()
  image_url: string;

  @ApiModelProperty()
  @Expose()
  frameTime: number;

  @ApiModelProperty()
  @Expose()
  detections: Track[];

  constructor(partial: Partial<FrameSerializer>) {
    Object.assign(this, partial);
  }
}
