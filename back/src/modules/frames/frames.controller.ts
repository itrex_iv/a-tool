import { Controller, Get, Param, Body, UseGuards, Put, ClassSerializerInterceptor, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetFramesForAllCamerasDto, GetFramesForCameraDto, UpdateFrameForCameraDto } from './dto';
import { Roles } from '../../core/auth/decorators/roles.decorator';
import { RolesGuard } from '../../core/auth/guards/role.guard';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UpdateFrameCommand } from './commands/impl';
import { FrameSerializer } from './serializers/frame.serializer';
import { GetFramesQuery, GetFrameQuery } from './queries/impl';
import { Frame } from './interfaces';
import { ApiUseTags, ApiBearerAuth, ApiOkResponse, ApiResponse } from '@nestjs/swagger';
import { InternalServerErrorOpenAPIResponse } from '../../shared/openapi/internal-server-error';
import { FrameNotFoundOpenAPIResponse } from './openapi/frame-not-found';
import { UserUnauthorizedErrorOpenAPIResponse } from '../../shared/openapi/user-unauthorized-error';
import { BadRequestErrorOpenAPIResponse } from '../../shared/openapi/bad-request-error';
import { FramesNotFoundOpenAPIResponse } from './openapi/frames-not-found';

@ApiBearerAuth()
@ApiUseTags('Frame')
@Controller('annotators')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class FramesController {
  constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {}

  // Commands
  @ApiOkResponse({ type: UpdateFrameForCameraDto })
  @ApiResponse(FrameNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Put(':annotator_id/sessions/:job_id/camera/:camera_id/frame/:index/command/putFrame')
  async updateFrameForCamera(@Param() paramDto: GetFramesForCameraDto, @Body() bodyDto: UpdateFrameForCameraDto) {
    await this.commandBus.execute(new UpdateFrameCommand(paramDto, bodyDto));
    return bodyDto;
  }

  // Queries
  @ApiOkResponse({ type: [FrameSerializer] })
  @ApiResponse(FramesNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':annotator_id/sessions/:job_id/frame/:index/query/getFrames')
  async getFramesForAllCameras(@Param() paramDto: GetFramesForAllCamerasDto) {
    const frames = await this.queryBus.execute<GetFramesQuery, Frame[]>(new GetFramesQuery(paramDto));
    return frames.map(frame => new FrameSerializer(frame));
  }

  @ApiOkResponse({ type: FrameSerializer })
  @ApiResponse(FrameNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':annotator_id/sessions/:job_id/camera/:camera_id/frame/:index/query/getFrame')
  async getFrameForCamera(@Param() paramDto: GetFramesForCameraDto) {
    return new FrameSerializer(await this.queryBus.execute<GetFrameQuery, Frame>(new GetFrameQuery(paramDto)));
  }
}
