import { GetFramesForCameraDto } from '../../dto';

export class GetFrameQuery {
  constructor(public readonly dto: GetFramesForCameraDto) {}
}
