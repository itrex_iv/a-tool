import { GetFramesForAllCamerasDto } from '../../dto';

export class GetFramesQuery {
  constructor(public readonly dto: GetFramesForAllCamerasDto) {}
}
