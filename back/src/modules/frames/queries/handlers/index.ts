import { GetFrameHandler } from './get-frame.handler';
import { GetFramesHandler } from './get-frames.handler';

export const queryHandlers = [GetFrameHandler, GetFramesHandler];
