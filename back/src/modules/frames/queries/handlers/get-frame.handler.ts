import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetFrameQuery } from '../impl';
import { Frame } from '../../interfaces';
import { FramesRepository } from '../../repository/frames.repository';
import { NotFoundException } from '@nestjs/common';
import { AppLogger } from '../../../../shared/logger/logger.service';
import { message } from '../../openapi/frame-not-found';

@QueryHandler(GetFrameQuery)
export class GetFrameHandler implements IQueryHandler<GetFrameQuery> {
  constructor(public readonly framesRepository: FramesRepository, private readonly appLogger: AppLogger) {}

  async execute({ dto }: GetFrameQuery): Promise<Frame> {
    this.appLogger.query(`GetFrame`);
    const { annotator_id, job_id, camera_id, index } = dto;

    const frameExist = await this.framesRepository.checkIfFrameExist(annotator_id, job_id, camera_id, index);
    if (!frameExist) throw new NotFoundException(message.FrameNotFound);

    const frame = await this.framesRepository.getFrame(annotator_id, job_id, camera_id, index);
    return { job_id, ...frame };
  }
}
