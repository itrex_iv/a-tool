import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetFramesQuery } from '../impl';
import { Frame } from '../../interfaces';
import { FilestorageService } from '../../../../core/filestorage/filestorage.service';
import { FramesRepository } from '../../repository/frames.repository';
import { NotFoundException } from '@nestjs/common';
import * as _ from 'lodash';
import { AppLogger } from '../../../../shared/logger/logger.service';
import { message } from '../../openapi/frames-not-found';

@QueryHandler(GetFramesQuery)
export class GetFramesHandler implements IQueryHandler<GetFramesQuery> {
  public readonly jobsDir: string;
  constructor(
    public readonly filestorageService: FilestorageService,
    public readonly framesRepository: FramesRepository,
    private readonly appLogger: AppLogger,
  ) {
    this.jobsDir = `${this.filestorageService.rootDir}/jobs`;
  }

  async execute({ dto }: GetFramesQuery): Promise<Frame[]> {
    this.appLogger.query(`GetFrames`);
    const { annotator_id, job_id, index } = dto;

    const cameraDirectory = `${this.jobsDir}/${job_id}/sessions/${annotator_id}/`;
    const framesExist = await this.filestorageService.checkFileDirExist(cameraDirectory);
    if (!framesExist) throw new NotFoundException(message.FramesNotFound);

    const camera_ids: string[] = await this.filestorageService.getDirContents(cameraDirectory);

    const frames: any = await Promise.all(
      camera_ids.map(async camera_id => {
        const frameExist = await this.framesRepository.checkIfFrameExist(annotator_id, job_id, Number(camera_id), index);
        if (!frameExist) return;

        const frame = await this.framesRepository.getFrame(annotator_id, job_id, Number(camera_id), index);
        return { job_id, ...frame };
      }),
    );
    const existFrames = _.filter(frames);
    if (existFrames.length === 0) throw new NotFoundException(message.FramesNotFound);
    return existFrames;
  }
}
