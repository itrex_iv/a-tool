import { Module } from '@nestjs/common';
import { FramesController } from './frames.controller';
import { commandHandlers } from './commands/handlers';
import { CqrsModule } from '@nestjs/cqrs';
import { queryHandlers } from './queries/handlers';
import { FilestorageModule } from '../../core/filestorage/filestorage.module';
import { FramesRepository } from './repository/frames.repository';
import { eventHandlers } from './events/handlers';

@Module({
  imports: [CqrsModule, FilestorageModule],
  controllers: [FramesController],
  providers: [...commandHandlers, ...queryHandlers, ...eventHandlers, FramesRepository],
  exports: [FramesRepository],
})
export class FramesModule {}
