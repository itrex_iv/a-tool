import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../../repository/annotators.repository';
import { SetSessionStatusCommand } from '../../impl';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(SetSessionStatusCommand)
export class SetSessionStatusHandler implements ICommandHandler<SetSessionStatusCommand> {
  constructor(private readonly annotatorsRepository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id, sessionStatus }: SetSessionStatusCommand) {
    this.appLogger.command(`SetSessionStatus: ${sessionStatus}`);
    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    const newAnnotator = {
      ...annotator,
      active_sessions: annotator.active_sessions.map(ses => (ses.job_id === job_id ? { ...ses, currentStatus: sessionStatus } : ses)),
    };

    await this.annotatorsRepository.save(newAnnotator);
  }
}
