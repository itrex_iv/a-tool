import { CommandHandler, ICommandHandler, QueryBus, EventBus } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../../repository/annotators.repository';
import { FinishSessionCommand } from '../../impl';
import { GetSessionQuery } from '../../../queries/impl';
import { NotFoundException, ConflictException } from '@nestjs/common';
import { currentStatus, Session } from '../../../interfaces';
import { AnnotationSessionFinishingEvent } from '../../../events/impl/annotation-session-finishing.event';
import { message as SessionNotFoundMessage } from '../../../openapi/session-not-found';
import { message as FinishSessionMessage } from '../../../openapi/finish-session-conflicts';
import { AppLogger } from '../../../../../shared/logger/logger.service';
import { message } from '../../../openapi/finish-session-conflicts';

@CommandHandler(FinishSessionCommand)
export class FinishSessionHandler implements ICommandHandler<FinishSessionCommand> {
  constructor(
    private readonly annotatorsRepository: AnnotatorsRepository,
    private readonly queryBus: QueryBus,
    private readonly eventBus: EventBus,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ annotator_id, job_id }: FinishSessionCommand) {
    this.appLogger.command(`FinishSession`);
    const session: Session = await this.queryBus.execute(new GetSessionQuery(annotator_id, job_id));
    if (!session) throw new NotFoundException(SessionNotFoundMessage.SessionNotFound);
    if (session.currentStatus === currentStatus.FinishingInProcess) throw new ConflictException(FinishSessionMessage.SessionInFinishing);
    if (session.currentStatus === currentStatus.SucceededInFinishing) throw new ConflictException(FinishSessionMessage.SessionAlreadyFinished);

    if (session.session_progress !== 100) throw new ConflictException(message.SessionProgressIsNotEnough);

    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    annotator.active_sessions = annotator.active_sessions.map(ses => {
      if (ses.job_id === job_id) ses.currentStatus = currentStatus.FinishingInProcess;
      return ses;
    });

    await this.annotatorsRepository.save(annotator);

    this.eventBus.publish(new AnnotationSessionFinishingEvent(annotator_id, job_id));
  }
}
