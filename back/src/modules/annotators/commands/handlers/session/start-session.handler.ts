import { CommandHandler, ICommandHandler, QueryBus, EventBus } from '@nestjs/cqrs';
import { StartSessionCommand } from '../../impl';
import { AnnotatorsRepository } from '../../../repository/annotators.repository';
import { ConflictException, NotFoundException } from '@nestjs/common';
import { GetSessionQuery } from '../../../queries/impl';
import { AnnotationSessionCreationInititationEvent } from '../../../events/impl/annotation-session-creation-inititation.event';
import { JobsRepository } from '../../../../jobs/repository/jobs.repository';
import { JobCreationInitiationEvent } from '../../../../jobs/events/impl/job-creation/job-creation-initiation.event';
import { currentStatus, Session } from '../../../interfaces';
import { message as StartSessionConflictMessage } from '../../../openapi/start-session-conflicts';
import { message as JobNotFoundMessage } from '../../../openapi/job-not-found';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(StartSessionCommand)
export class StartSessionHandler implements ICommandHandler<StartSessionCommand> {
  constructor(
    private readonly annotatorsRepository: AnnotatorsRepository,
    private readonly jobsRepository: JobsRepository,
    private readonly queryBus: QueryBus,
    private readonly eventBus: EventBus,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ annotator_id, job_id }: StartSessionCommand) {
    this.appLogger.command(`StartSession`);
    const session = await this.queryBus.execute(new GetSessionQuery(annotator_id, job_id));
    if (session) throw new ConflictException(StartSessionConflictMessage.SessionAlreadyExists);

    const job = await this.jobsRepository.getJobById(job_id);
    if (!job) throw new NotFoundException(JobNotFoundMessage.JobNotFound);

    const newSession: Partial<Session> = {
      job_id,
      currentStatus: currentStatus.StartingInProcess,
      ...(job.detections_qty
        ? {
            session_progress: Math.round((job.detections_with_track_id / job.detections_qty) * 100000) / 1000,
            detections_qty: job.detections_qty,
            detections_with_track_id: job.detections_with_track_id,
          }
        : {}),
    };

    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    annotator.active_sessions.push(newSession as Session);
    await this.annotatorsRepository.save(annotator);

    const jobExists = await this.jobsRepository.checkIfJobExists(job_id);
    if (!jobExists) this.eventBus.publish(new JobCreationInitiationEvent(annotator_id, job_id));
    else this.eventBus.publish(new AnnotationSessionCreationInititationEvent(annotator_id, job_id));
  }
}
