import { CommandHandler, ICommandHandler, CommandBus } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../../../repository/annotators.repository';
import { MarkSessionFinishingSucceedCommand } from '../../../impl';
import { currentStatus } from '../../../../interfaces';
import { AppLogger } from '../../../../../../shared/logger/logger.service';

@CommandHandler(MarkSessionFinishingSucceedCommand)
export class MarkSessionFinishingSucceedHandler implements ICommandHandler<MarkSessionFinishingSucceedCommand> {
  constructor(private readonly annotatorsRepository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id }: MarkSessionFinishingSucceedCommand) {
    this.appLogger.command(`MarkSessionFinishingSucceed`);
    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    annotator.active_sessions = annotator.active_sessions.map(ses => {
      if (ses.job_id === job_id) ses.currentStatus = currentStatus.SucceededInFinishing;
      return ses;
    });

    await this.annotatorsRepository.save(annotator);
  }
}
