import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../../../repository/annotators.repository';
import { MarkSessionFinishingFailedCommand } from '../../../impl';
import { currentStatus } from '../../../../interfaces';
import { AppLogger } from '../../../../../../shared/logger/logger.service';

@CommandHandler(MarkSessionFinishingFailedCommand)
export class MarkSessionFinishingFailedHandler implements ICommandHandler<MarkSessionFinishingFailedCommand> {
  constructor(private readonly annotatorsRepository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id }: MarkSessionFinishingFailedCommand) {
    this.appLogger.command(`MarkSessionFinishingFailed`);
    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    annotator.active_sessions = annotator.active_sessions.map(ses => {
      if (ses.job_id === job_id) ses.currentStatus = currentStatus.FailedInFinishing;
      return ses;
    });

    await this.annotatorsRepository.save(annotator);
  }
}
