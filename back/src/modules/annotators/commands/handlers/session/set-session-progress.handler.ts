import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../../repository/annotators.repository';
import { SetSessionProgressCommand } from '../../impl';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(SetSessionProgressCommand)
export class SetSessionProgressHandler implements ICommandHandler<SetSessionProgressCommand> {
  constructor(private readonly annotatorsRepository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id, detections_qty, detections_with_track_id }: SetSessionProgressCommand) {
    this.appLogger.command(`SetSessionProgress: ${detections_qty} ${detections_with_track_id}`);
    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    const newAnnotator = {
      ...annotator,
      active_sessions: annotator.active_sessions.map(ses =>
        ses.job_id === job_id
          ? {
              ...ses,
              detections_qty,
              detections_with_track_id,
              session_progress: Math.round((detections_with_track_id / detections_qty) * 100000) / 1000,
            }
          : ses,
      ),
    };

    await this.annotatorsRepository.save(newAnnotator);
  }
}
