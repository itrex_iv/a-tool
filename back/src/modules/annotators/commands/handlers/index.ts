import { FinishSessionHandler } from './session/finish-session.handler';
import { StartSessionHandler } from './session/start-session.handler';
import { MarkSessionFinishingFailedHandler } from './session/mark-session-finishing/mark-session-finishing-failed.handler';
import { MarkSessionFinishingSucceedHandler } from './session/mark-session-finishing/mark-session-finishing-succeed.handler';
import { SetSessionStatusHandler } from './session/set-session-status.handler';
import { SetSessionProgressHandler } from './session/set-session-progress.handler';

export const commandHandlers = [
  FinishSessionHandler,
  StartSessionHandler,
  SetSessionStatusHandler,
  MarkSessionFinishingFailedHandler,
  MarkSessionFinishingSucceedHandler,
  SetSessionProgressHandler,
];
