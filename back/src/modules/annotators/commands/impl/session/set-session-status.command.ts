import { currentStatus } from '../../../interfaces';

export class SetSessionStatusCommand {
  constructor(public readonly annotator_id: string, public readonly job_id: string, public readonly sessionStatus: currentStatus) {}
}
