import { Model, Types } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Annotator, MongooseAnnotator } from '../interfaces';

@Injectable()
export class AnnotatorsRepository {
  constructor(
    @Inject('ANNOTATOR_MODEL')
    private readonly annotatorModel: Model<MongooseAnnotator>,
  ) {}

  async findOneById(annotator_id: string): Promise<Annotator> {
    if (!Types.ObjectId.isValid(annotator_id)) return;
    const annotator = await this.annotatorModel.findOne({ _id: annotator_id }).exec();
    if (annotator) return annotator.toObject();
  }

  async findOneByUsername(username: string): Promise<Annotator> {
    const annotator = await this.annotatorModel.findOne({ username }).exec();
    if (annotator) return annotator.toObject();
  }

  async save(annotator: Annotator): Promise<unknown> {
    return this.annotatorModel.findOneAndUpdate({ _id: annotator._id }, annotator, { upset: true });
  }
}
