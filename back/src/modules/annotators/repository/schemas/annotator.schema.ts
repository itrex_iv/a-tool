import * as mongoose from 'mongoose';

export const annotatorSchema = new mongoose.Schema(
  {
    first_name: String,
    last_name: String,
    username: String,
    password: String,
    roles: [String],
    active_sessions: [
      {
        job_id: String,
        session_progress: Number,
        detections_qty: Number,
        detections_with_track_id: Number,
        currentStatus: String,
      },
    ],
  },
  { timestamps: true },
);
