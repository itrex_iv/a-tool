import { ApiModelProperty } from '@nestjs/swagger';

export class FinishSessionSerializer {
  @ApiModelProperty({ example: 'Session finishing is in process!' })
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
