import { Exclude, Expose } from 'class-transformer';
import { currentStatus } from '../interfaces';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class SessionStatusSerializer {
  @ApiModelProperty({ enum: currentStatus })
  @Expose()
  status: currentStatus;

  constructor(partial: Partial<SessionStatusSerializer>) {
    Object.assign(this, partial);
  }
}
