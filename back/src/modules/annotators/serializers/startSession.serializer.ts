import { ApiModelProperty } from '@nestjs/swagger';

export class StartSessionSerializer {
  @ApiModelProperty({ example: 'Successfully start session!' })
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}
