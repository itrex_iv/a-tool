import { Exclude, Transform, Expose } from 'class-transformer';
import { Session } from '../interfaces';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class AnnotatorSerializer {
  @Transform(value => String(value))
  @Expose({ name: 'annotator_id' })
  _id: string;

  @ApiModelProperty()
  private annotator_id: string; // internal use for swagger

  @ApiModelProperty()
  @Expose()
  first_name: string;

  @ApiModelProperty()
  @Expose()
  last_name: string;

  @ApiModelProperty()
  @Expose()
  username: string;

  @Exclude()
  active_sessions: Session[];

  @Exclude()
  roles: string[];

  @Exclude()
  password: string;

  @Exclude()
  updatedAt: string;

  constructor(partial: Partial<AnnotatorSerializer>) {
    Object.assign(this, partial);
  }
}
