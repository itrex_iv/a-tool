export enum currentStatus {
  StartingInProcess = 'StartingInProcess',
  FailedInStarting = 'FailedInStarting',
  SucceededInStarting = 'SucceededInStarting',
  FinishingInProcess = 'FinishingInProcess',
  FailedInFinishing = 'FailedInFinishing',
  SucceededInFinishing = 'SucceededInFinishing',
}

export interface Annotator {
  _id: string;
  username: string;
  password: string;
  roles: string[];
  first_name: string;
  last_name: string;
  active_sessions: Session[];
}

export interface Session {
  job_id: string;
  session_progress: number;
  detections_qty: number;
  detections_with_track_id: number;
  currentStatus: currentStatus;
}

export interface MongooseAnnotator extends Annotator, Document {}
