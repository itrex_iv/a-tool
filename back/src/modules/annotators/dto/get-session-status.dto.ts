import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class GetSessionStatusDto {
  @ApiModelProperty()
  @IsString()
  public readonly annotator_id: string;

  @ApiModelProperty()
  @IsString()
  public readonly job_id: string;
}
