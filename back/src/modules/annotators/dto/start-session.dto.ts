import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class StartSessionDto {
  @ApiModelProperty()
  @IsString()
  @Transform(value => String(value))
  public readonly job_id: string;
}
