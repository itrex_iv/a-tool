import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export class FinishSessionDto {
  @ApiModelProperty()
  @IsString()
  @Transform(value => String(value))
  public readonly job_id: string;
}
