import { GetAnnotatorHandler } from './get-annotator.handler';
import { GetSessionHandler } from './get-session.handler';
import { GetAnnotatorByIdHandler } from './get-annotator-byId.handler';

export const queryHandlers = [GetAnnotatorHandler, GetSessionHandler, GetAnnotatorByIdHandler];
