import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../repository/annotators.repository';
import { GetAnnotatorQuery } from '../impl';
import { Annotator } from '../../interfaces';
import { AppLogger } from '../../../../shared/logger/logger.service';

@QueryHandler(GetAnnotatorQuery)
export class GetAnnotatorHandler implements IQueryHandler<GetAnnotatorQuery> {
  constructor(private readonly repository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ username }: GetAnnotatorQuery): Promise<Annotator> {
    this.appLogger.query(`GetAnnotator`);
    return this.repository.findOneByUsername(username);
  }
}
