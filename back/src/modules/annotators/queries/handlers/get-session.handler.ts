import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../repository/annotators.repository';
import { GetSessionQuery } from '../impl';
import { Session } from '../../interfaces';
import { NotFoundException } from '@nestjs/common';
import { message } from '../../openapi/annotator-not-found';
import { AppLogger } from '../../../../shared/logger/logger.service';

@QueryHandler(GetSessionQuery)
export class GetSessionHandler implements IQueryHandler<GetSessionQuery> {
  constructor(private readonly annotatorsRepository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id }: GetSessionQuery): Promise<Session> {
    this.appLogger.query(`GetSession`);
    const annotator = await this.annotatorsRepository.findOneById(annotator_id);
    if (!annotator) throw new NotFoundException(message.AnnotatorNotFound);
    return annotator.active_sessions.find(ses => ses.job_id === job_id);
  }
}
