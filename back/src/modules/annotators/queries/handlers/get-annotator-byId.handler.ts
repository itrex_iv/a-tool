import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../repository/annotators.repository';
import { GetAnnotatorByIdQuery } from '../impl';
import { Annotator } from '../../interfaces';
import { NotFoundException } from '@nestjs/common';
import { message } from '../../openapi/annotator-not-found';
import { AppLogger } from '../../../../shared/logger/logger.service';

@QueryHandler(GetAnnotatorByIdQuery)
export class GetAnnotatorByIdHandler implements IQueryHandler<GetAnnotatorByIdQuery> {
  constructor(private readonly repository: AnnotatorsRepository, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id }: GetAnnotatorByIdQuery): Promise<Annotator> {
    this.appLogger.query(`GetAnnotatorById`);
    const annotator = await this.repository.findOneById(annotator_id);
    if (!annotator) throw new NotFoundException(message.AnnotatorNotFound);
    return annotator;
  }
}
