import {
  Controller,
  Post,
  Get,
  Param,
  Body,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor,
  Request,
  NotFoundException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Annotator, Session } from './interfaces';
import { GetSessionQuery, GetAnnotatorByIdQuery } from './queries/impl';
import { AuthGuard } from '@nestjs/passport';
import { AnnotatorSerializer } from './serializers/annotator.serializer';
import { StartSessionDto, FinishSessionDto, GetSessionStatusDto } from './dto';
import { StartSessionCommand, FinishSessionCommand } from './commands/impl';
import { Roles } from '../../core/auth/decorators/roles.decorator';
import { RolesGuard } from '../../core/auth/guards/role.guard';
import { SessionStatusSerializer } from './serializers/sessionStatus.serializer';
import { StartSessionSerializer } from './serializers/startSession.serializer';
import { FinishSessionSerializer } from './serializers/finishSession.serializer';
import { ApiUseTags, ApiBearerAuth, ApiOkResponse, ApiResponse } from '@nestjs/swagger';

import { InternalServerErrorOpenAPIResponse } from '../../shared/openapi/internal-server-error';
import { AnnotatorNotFoundOpenAPIResponse } from './openapi/annotator-not-found';
import { JobNotFoundOpenAPIResponse } from './openapi/job-not-found';
import { UserUnauthorizedErrorOpenAPIResponse } from '../../shared/openapi/user-unauthorized-error';
import { SessionNotFoundOpenAPIResponse, message as SessionNotFoundMessage } from './openapi/session-not-found';
import { FinishSessionConflictsOpenAPIResponse } from './openapi/finish-session-conflicts';
import { StartSessionConflictsOpenAPIResponse } from './openapi/start-session-conflicts';
import { BadRequestErrorOpenAPIResponse } from '../../shared/openapi/bad-request-error';

@ApiBearerAuth()
@ApiUseTags('Annotator')
@Controller('annotators')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class AnnotatorsController {
  constructor(private readonly commandBus: CommandBus, private readonly queryBus: QueryBus) {}

  // Commands
  @ApiOkResponse({ type: StartSessionSerializer })
  @ApiResponse(StartSessionConflictsOpenAPIResponse)
  @ApiResponse(JobNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Post(':annotator_id/command/startSession')
  async startSession(@Param('annotator_id') annotator_id: string, @Body() dto: StartSessionDto) {
    await this.commandBus.execute(new StartSessionCommand(annotator_id, dto.job_id));
    return new StartSessionSerializer('Successfully start session!');
  }

  @ApiOkResponse({ type: FinishSessionSerializer })
  @ApiResponse(FinishSessionConflictsOpenAPIResponse)
  @ApiResponse(SessionNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Post(':annotator_id/command/finishSession')
  async finishSession(@Param('annotator_id') annotator_id: string, @Body() dto: FinishSessionDto) {
    await this.commandBus.execute(new FinishSessionCommand(annotator_id, dto.job_id));
    return new FinishSessionSerializer('Session finishing is in process!');
  }

  // Queries
  @ApiOkResponse({ type: AnnotatorSerializer })
  @ApiResponse(AnnotatorNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('query/getAnnotator')
  async getAnnotator(@Request() req: any): Promise<Annotator> {
    return new AnnotatorSerializer(await this.queryBus.execute<GetAnnotatorByIdQuery, Annotator>(new GetAnnotatorByIdQuery(req.user.userId)));
  }

  @ApiOkResponse({ type: SessionStatusSerializer })
  @ApiResponse(SessionNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':annotator_id/sessions/:job_id/query/getSessionStatus')
  async getFramesForAllCameras(@Param() paramDto: GetSessionStatusDto) {
    const session = await this.queryBus.execute<GetSessionQuery, Session>(new GetSessionQuery(paramDto.annotator_id, paramDto.job_id));
    if (!session) throw new NotFoundException(SessionNotFoundMessage.SessionNotFound);

    return new SessionStatusSerializer({ status: session.currentStatus });
  }
}
