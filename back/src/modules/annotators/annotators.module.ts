import { Module } from '@nestjs/common';
import { AnnotatorsController } from './annotators.controller';
import { commandHandlers } from './commands/handlers';
import { eventHandlers } from './events/handlers';
import { queryHandlers } from './queries/handlers';
import { SessionCreationSagas } from './sagas/session-creation.sagas';
import { AnnotatorsRepository } from './repository/annotators.repository';
import { CqrsModule } from '@nestjs/cqrs';
import { DatabaseModule } from '../../core/database/database.module';
import { annotatorsProviders } from './providers/annotators.providers';
import { FilestorageModule } from '../../core/filestorage/filestorage.module';
import { JobsModule } from '../jobs/jobs.module';
import { SessionArchivingSagas } from './sagas/session-archiving.sagas';
import { SessionSagas } from './sagas/session.sagas';

@Module({
  imports: [CqrsModule, DatabaseModule, FilestorageModule, JobsModule],
  controllers: [AnnotatorsController],
  providers: [
    AnnotatorsRepository,
    ...commandHandlers,
    ...eventHandlers,
    ...queryHandlers,
    SessionSagas,
    SessionCreationSagas,
    SessionArchivingSagas,
    ...annotatorsProviders,
  ],
})
export class AnnotatorsModule {}
