import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  SessionNotFound = 'Session not found!',
  AnnotatorNotFound = 'Annotator not found!',
}

export class SessionNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const SessionNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: SessionNotFoundOpenAPIResponseType,
};
