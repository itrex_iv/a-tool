import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  SessionAlreadyFinished = 'Session has already finished!',
  SessionProgressIsNotEnough = "Can't finish the session with progress less than 100!",
  SessionInFinishing = 'Session finishing is in process!',
}

export class FinishSessionConflictsOpenAPIResponseType {
  @ApiModelProperty({ example: 'Conflict' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const FinishSessionConflictsOpenAPIResponse = {
  status: HttpStatus.CONFLICT,
  type: FinishSessionConflictsOpenAPIResponseType,
};
