import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  SessionAlreadyExists = 'Session has already Started!',
}

export class StartSessionConflictsOpenAPIResponseType {
  @ApiModelProperty({ example: 'Conflict' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const StartSessionConflictsOpenAPIResponse = {
  status: HttpStatus.CONFLICT,
  type: StartSessionConflictsOpenAPIResponseType,
};
