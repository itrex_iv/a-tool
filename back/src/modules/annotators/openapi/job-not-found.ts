import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  JobNotFound = 'Job not found!',
  AnnotatorNotFound = 'Annotator not found!',
}

export class JobNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const JobNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: JobNotFoundOpenAPIResponseType,
};
