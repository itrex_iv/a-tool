import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { AnnotationSessionFinishingEvent } from '../impl/annotation-session-finishing.event';
import { AppLogger } from '../../../../shared/logger/logger.service';

@EventsHandler(AnnotationSessionFinishingEvent)
export class AnnotationSessionFinishingHandler implements IEventHandler<AnnotationSessionFinishingEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: AnnotationSessionFinishingEvent) {
    this.appLogger.event(`AnnotationSessionFinishing`);
    return;
  }
}
