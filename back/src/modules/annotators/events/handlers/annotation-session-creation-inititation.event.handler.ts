import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { AnnotationSessionCreationInititationEvent } from '../impl/annotation-session-creation-inititation.event';
import { AppLogger } from '../../../../shared/logger/logger.service';

@EventsHandler(AnnotationSessionCreationInititationEvent)
export class AnnotationSessionCreationInititationHandler implements IEventHandler<AnnotationSessionCreationInititationEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: AnnotationSessionCreationInititationEvent) {
    this.appLogger.event(`AnnotationSessionCreationInititation`);
    return;
  }
}
