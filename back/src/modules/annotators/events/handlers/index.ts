import { AnnotationSessionCreationInititationHandler } from './annotation-session-creation-inititation.event.handler';
import { AnnotationSessionFinishingHandler } from './annotation-session-finishing.event.handler';

export const eventHandlers = [AnnotationSessionCreationInititationHandler, AnnotationSessionFinishingHandler];
