import { Connection } from 'mongoose';
import { annotatorSchema } from '../repository/schemas/annotator.schema';

export const annotatorsProviders = [
  {
    provide: 'ANNOTATOR_MODEL',
    useFactory: (connection: Connection) => connection.model('annotators', annotatorSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
