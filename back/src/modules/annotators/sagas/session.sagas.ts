import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { SetSessionProgressCommand } from '../commands/impl';
import { AnnotationSessionUpdatingEvent } from '../../frames/events/impl/annotation-session-finishing.event';

@Injectable()
export class SessionSagas {
  @Saga()
  updatingSession = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(AnnotationSessionUpdatingEvent),
      delay(1000),
      map(event => {
        return new SetSessionProgressCommand(event.annotator_id, event.job_id, event.detections_qty, event.detections_with_track_id);
      }),
    );
  };
}
