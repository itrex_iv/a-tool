import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { AnnotationSessionCreationInititationEvent } from '../events/impl/annotation-session-creation-inititation.event';
import { CopySessionCommand } from '../../jobs/commands/impl';
import { AnnotationSessionCreationFailureEvent } from '../../jobs/events/impl/annotation-session-creation/annotation-session-creation-failure.event';
import { AnnotationSessionCreationSuccessEvent } from '../../jobs/events/impl/annotation-session-creation/annotation-session-creation-success.event';
import { SetSessionStatusCommand, SetSessionProgressCommand } from '../commands/impl';
import { currentStatus } from '../interfaces';
import { DownloadImageSuccessEvent } from '../../images/events/impl/download-image-success.event';
import { DownloadMetaSuccessEvent } from '../../jobs/events/impl/download-meta/download-meta-success.event';

@Injectable()
export class SessionCreationSagas {
  @Saga()
  copySessionImmediately = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(AnnotationSessionCreationInititationEvent),
      delay(1000),
      map(event => {
        return new CopySessionCommand(event.annotator_id, event.job_id);
      }),
    );
  };

  @Saga()
  sessionMetaDownloadSucceed = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadMetaSuccessEvent),
      delay(1000),
      map(event => {
        return new SetSessionProgressCommand(event.annotator_id, event.job_id, event.detections_qty, event.detections_with_track_id);
      }),
    );
  };

  @Saga()
  copySessionAfterJobCreation = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadImageSuccessEvent),
      delay(1000),
      map(event => {
        return new CopySessionCommand(event.annotator_id, event.job_id);
      }),
    );
  };

  @Saga()
  sessionCreationFailed = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(AnnotationSessionCreationFailureEvent),
      delay(1000),
      map(event => {
        return new SetSessionStatusCommand(event.annotator_id, event.job_id, currentStatus.FailedInStarting);
      }),
    );
  };

  @Saga()
  sessionCreationSucceed = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(AnnotationSessionCreationSuccessEvent),
      delay(1000),
      map(event => {
        return new SetSessionStatusCommand(event.annotator_id, event.job_id, currentStatus.SucceededInStarting);
      }),
    );
  };
}
