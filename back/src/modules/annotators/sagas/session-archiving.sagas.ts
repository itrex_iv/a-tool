import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { MarkSessionFinishingFailedCommand, MarkSessionFinishingSucceedCommand } from '../commands/impl';
import { SendAnnotationFailureEvent } from '../../annotations/events/impl/send-annotation-failure.event';
import { SendAnnotationSuccessEvent } from '../../annotations/events/impl/send-annotation-success.event';

@Injectable()
export class SessionArchivingSagas {
  @Saga()
  sessionArchivingFailed = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(SendAnnotationFailureEvent),
      delay(1000),
      map(event => {
        return new MarkSessionFinishingFailedCommand(event.annotator_id, event.job_id);
      }),
    );
  };

  @Saga()
  sessionArchivingSucceed = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(SendAnnotationSuccessEvent),
      delay(1000),
      map(event => {
        return new MarkSessionFinishingSucceedCommand(event.annotator_id, event.job_id);
      }),
    );
  };
}
