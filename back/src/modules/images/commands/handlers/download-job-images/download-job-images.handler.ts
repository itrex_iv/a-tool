import { fork } from 'child_process';
import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { DownloadJobImagesCommand } from '../../impl';
import { ConfigService } from '../../../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../../../shared/config/constants';
import { DownloadImageFailureEvent } from '../../../events/impl/download-image-failure.event';
import { DownloadImageSuccessEvent } from '../../../events/impl/download-image-success.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(DownloadJobImagesCommand)
export class DownloadJobImagesHandler implements ICommandHandler<DownloadJobImagesCommand> {
  constructor(private readonly configService: ConfigService, private readonly eventBus: EventBus, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, job_id }: DownloadJobImagesCommand) {
    this.appLogger.command(`DownloadJobImages`);
    try {
      const forked = fork(`${__dirname}/download-job-images.fork.js`);
      forked.send({
        job_id,
        baseURL: this.configService.get(ENV_VARIABLE_KEY.JOB_SERVER_DOMAIN),
        baseDir: this.configService.get(ENV_VARIABLE_KEY.FS_ROOT_DIR),
      });

      forked.on('message', msg => {
        if (msg.error) {
          console.log(msg);
          this.eventBus.publish(new DownloadImageFailureEvent(annotator_id, job_id));
        } else this.eventBus.publish(new DownloadImageSuccessEvent(annotator_id, job_id));
      });
    } catch (e) {
      console.log(e);
      this.eventBus.publish(new DownloadImageFailureEvent(annotator_id, job_id));
    }
  }
}
