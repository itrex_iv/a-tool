import * as fs from 'fs-extra';
import axios from 'axios';
import * as unzip from 'unzip-stream';

process.on('message', async ({ baseURL, job_id, baseDir }) => {
  try {
    const { data } = await axios.get(`jobs/${job_id}/images`, { baseURL, responseType: 'stream' });
    data.pipe(unzip.Extract({ path: `${baseDir}/${job_id}` })).on('finish', async () => {
      await fs.move(`${baseDir}/${job_id}/image`, `${baseDir}/images/${job_id}`);
      await fs.remove(`${baseDir}/${job_id}`);
      process.send({ error: false });
    });
  } catch (e) {
    process.send({ error: true, msg: e });
  }
});
