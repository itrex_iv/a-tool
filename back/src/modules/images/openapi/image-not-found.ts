import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  ImageNotFound = 'Image not found!',
}

export class ImageNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const ImageNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: ImageNotFoundOpenAPIResponseType,
};
