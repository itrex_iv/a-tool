import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class GetImageDto {
  @ApiModelProperty()
  @IsString()
  public readonly annotator_id: string;

  @ApiModelProperty()
  @IsString()
  public readonly job_id: string;

  @ApiModelProperty()
  @IsString()
  public readonly image_url: string;
}
