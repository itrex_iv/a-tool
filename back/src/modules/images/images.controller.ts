import { Controller, UseInterceptors, ClassSerializerInterceptor, Get, Param, Res } from '@nestjs/common';
import { Roles } from '../../core/auth/decorators/roles.decorator';
import { GetImageDto } from './dto';
import { ImagesRepository } from './repository/images.repository';
import { Response } from 'express';
import { ApiUseTags, ApiOkResponse, ApiResponse } from '@nestjs/swagger';
import { InternalServerErrorOpenAPIResponse } from '../../shared/openapi/internal-server-error';
import { ImageNotFoundOpenAPIResponse } from './openapi/image-not-found';
import { BadRequestErrorOpenAPIResponse } from '../../shared/openapi/bad-request-error';

@ApiUseTags('Image')
@Controller('annotators')
export class ImagesController {
  constructor(private readonly imageRepository: ImagesRepository) {}

  @ApiOkResponse({ description: 'Stream of jpg data' })
  @ApiResponse(ImageNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':annotator_id/sessions/:job_id/images/:image_url/query/getImage')
  async getImage(@Res() res: Response, @Param() paramDto: GetImageDto) {
    const { job_id, image_url } = paramDto;
    const imageStream = await this.imageRepository.getImageStream(job_id, image_url);
    imageStream.pipe(res);
    imageStream.on('end', () => res.end());
  }
}
