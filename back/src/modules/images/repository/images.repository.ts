import { Injectable, NotFoundException } from '@nestjs/common';
import { FilestorageService } from '../../../core/filestorage/filestorage.service';
import { message } from '../openapi/image-not-found';

@Injectable()
export class ImagesRepository {
  private readonly imagesDir: string;
  constructor(private readonly filestorageService: FilestorageService) {
    this.imagesDir = `${this.filestorageService.rootDir}/images`;
  }

  async getImageStream(job_id: string, image_url: string): Promise<any> {
    const pathToImage = `${this.imagesDir}/${job_id}/${image_url}`;
    if (!(await this.filestorageService.checkFileDirExist(pathToImage))) {
      throw new NotFoundException(message.ImageNotFound);
    }

    return this.filestorageService.createReadStream(pathToImage);
  }
}
