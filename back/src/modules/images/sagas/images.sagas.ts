import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { DownloadJobImagesCommand } from '../commands/impl';
import { DownloadMetaSuccessEvent } from '../../jobs/events/impl/download-meta/download-meta-success.event';

@Injectable()
export class ImagesSagas {
  @Saga()
  downloadJobImage = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadMetaSuccessEvent),
      delay(1000),
      map(event => {
        return new DownloadJobImagesCommand(event.annotator_id, event.job_id);
      }),
    );
  };
}
