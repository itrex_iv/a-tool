import { Module } from '@nestjs/common';
import { ImagesController } from './images.controller';
import { ImagesRepository } from './repository/images.repository';
import { FilestorageModule } from '../../core/filestorage/filestorage.module';
import { commandHandlers } from './commands/handlers';
import { CqrsModule } from '@nestjs/cqrs';
import { eventHandlers } from './events/handlers';
import { ImagesSagas } from './sagas/images.sagas';

@Module({
  imports: [CqrsModule, FilestorageModule],
  controllers: [ImagesController],
  providers: [ImagesRepository, ...commandHandlers, ...eventHandlers, ImagesSagas],
})
export class ImagesModule {}
