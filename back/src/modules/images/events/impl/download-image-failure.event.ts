export class DownloadImageFailureEvent {
  constructor(public readonly annotator_id: string, public readonly job_id: string) {}
}
