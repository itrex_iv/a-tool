import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { DownloadImageSuccessEvent } from '../../impl/download-image-success.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(DownloadImageSuccessEvent)
export class DownloadImageSuccessHandler implements IEventHandler<DownloadImageSuccessEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: DownloadImageSuccessEvent) {
    this.appLogger.event(`DownloadImageSuccess`);
    return;
  }
}
