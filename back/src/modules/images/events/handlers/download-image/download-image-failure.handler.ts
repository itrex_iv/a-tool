import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { DownloadImageFailureEvent } from '../../impl/download-image-failure.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(DownloadImageFailureEvent)
export class DownloadImageFailureHandler implements IEventHandler<DownloadImageFailureEvent> {
  constructor(private readonly appLogger: AppLogger) {}
  handle(event: DownloadImageFailureEvent) {
    this.appLogger.event(`DownloadImageFailure`);
    return;
  }
}
