import { DownloadImageFailureHandler } from './download-image/download-image-failure.handler';
import { DownloadImageSuccessHandler } from './download-image/download-image-success.handler';

export const eventHandlers = [DownloadImageFailureHandler, DownloadImageSuccessHandler];
