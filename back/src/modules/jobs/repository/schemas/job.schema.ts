import * as mongoose from 'mongoose';

export const jobSchema = new mongoose.Schema(
  {
    session: Number,
    job_id: String,
    job_name: String,
    duration: Number,
    frame_rate: Number,
    start_date: Number,
    end_date: Number,
    camera_info: [
      {
        _id: false,
        camera_id: Number,
        name: String,
        image_width: Number,
        image_height: Number,
        frames_count: Number,
      },
    ],
    detections_qty: Number,
    detections_with_track_id: Number,
    currentStatus: String,
  },
  { timestamps: true },
);
