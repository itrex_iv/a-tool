import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Job, MongooseJob } from '../interfaces';
import { FilestorageService } from '../../../core/filestorage/filestorage.service';

@Injectable()
export class JobsRepository {
  public readonly jobsDir: string;

  constructor(
    @Inject('JOB_MODEL')
    private readonly jobModel: Model<MongooseJob>,
    private readonly filestorageService: FilestorageService,
  ) {
    this.jobsDir = `${this.filestorageService.rootDir}/jobs`;
  }

  async checkIfJobExists(jobId: string): Promise<boolean> {
    const pathToRaw = `${this.jobsDir}/${jobId}/raw`;
    return this.filestorageService.checkFileDirExist(pathToRaw);
  }

  async getJobsInTimeRange(from: number, to: number, skip: number, limit: number): Promise<Job[]> {
    const jobs = await this.jobModel
      .find({ start_date: { $gte: from, $lte: to } })
      .sort({ start_date: -1 })
      .skip(skip)
      .limit(limit)
      .exec();
    return jobs.map(job => job.toObject());
  }

  async getAllJobsIds(): Promise<Job[]> {
    const jobs = await this.jobModel
      .find()
      .select('job_id')
      .exec();
    return jobs.map(job => job.toObject());
  }

  async getJobById(job_id: string): Promise<Job> {
    const job = await this.jobModel.findOne({ job_id }).exec();
    if (job) return job.toObject();
  }

  async create(jobToCreate: Job) {
    const createdJob = new this.jobModel(jobToCreate);
    return createdJob.save();
  }

  async save(job: Job): Promise<unknown> {
    return this.jobModel.findOneAndUpdate({ job_id: job.job_id }, job, { upset: true });
  }
}
