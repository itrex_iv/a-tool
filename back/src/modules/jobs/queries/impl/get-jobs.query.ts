export class GetJobsQuery {
  constructor(
    public readonly annotator_id: string,
    public readonly from: number,
    public readonly to: number,
    public readonly page: number,
    public readonly per_page: number,
  ) {}
}
