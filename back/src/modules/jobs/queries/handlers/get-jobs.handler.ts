import { IQueryHandler, QueryHandler, QueryBus } from '@nestjs/cqrs';
import { GetJobsQuery } from '../impl';
import { Job } from '../../interfaces';
import { JobsRepository } from '../../repository/jobs.repository';
import { GetAnnotatorByIdQuery } from '../../../annotators/queries/impl';
import { Annotator, currentStatus } from '../../../annotators/interfaces';
import { AppLogger } from '../../../../shared/logger/logger.service';

@QueryHandler(GetJobsQuery)
export class GetJobsHandler implements IQueryHandler<GetJobsQuery> {
  constructor(private readonly repository: JobsRepository, private readonly queryBus: QueryBus, private readonly appLogger: AppLogger) {}

  async execute({ annotator_id, from, to, page, per_page }: GetJobsQuery): Promise<Job[]> {
    this.appLogger.query(`GetJobs`);
    const annotator = await this.queryBus.execute<GetAnnotatorByIdQuery, Annotator>(new GetAnnotatorByIdQuery(annotator_id));

    const skip = (page - 1) * per_page;
    const jobs: Job[] = await this.repository.getJobsInTimeRange(from, to, skip, per_page);

    return jobs.reduce((acc, job: Job) => {
      const jobInProgress = annotator.active_sessions.find(ses => ses.job_id === job.job_id);
      if (jobInProgress && jobInProgress.currentStatus === currentStatus.SucceededInFinishing) return acc;
      if (jobInProgress) job.job_progress = jobInProgress.session_progress;
      return acc.concat([job]);
    }, []);
  }
}
