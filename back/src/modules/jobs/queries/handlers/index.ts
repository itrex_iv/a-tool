import { GetJobsHandler } from './get-jobs.handler';
import { DownloadJobsListHandler } from './download-jobs-list.handler';

export const queryHandlers = [GetJobsHandler, DownloadJobsListHandler];
