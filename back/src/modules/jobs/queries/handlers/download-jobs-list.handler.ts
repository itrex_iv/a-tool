import { IQueryHandler, QueryHandler, QueryBus } from '@nestjs/cqrs';
import { DownloadJobsListQuery } from '../impl';
import { DownloadedJob } from '../../dto/downloaded-job.dto';
import { plainToClass, ClassTransformOptions } from 'class-transformer';
import { Job } from '../../interfaces';
import { HttpService } from '@nestjs/common';
import { AppLogger } from '../../../../shared/logger/logger.service';

@QueryHandler(DownloadJobsListQuery)
export class DownloadJobsListHandler implements IQueryHandler<DownloadJobsListQuery> {
  constructor(private readonly httpService: HttpService, private readonly appLogger: AppLogger) {}

  async execute({  }: DownloadJobsListQuery): Promise<Job[]> {
    this.appLogger.query(`DownloadJobsList`);
    try {
      const { data } = await this.httpService.get('jobs').toPromise();
      const options: ClassTransformOptions = { strategy: 'excludeAll' };
      return data.map((jobFixture: Job) => plainToClass(DownloadedJob, jobFixture, options));
    } catch (e) {
      this.appLogger.query(`DownloadJobsList:Failed`);
      return [];
    }
  }
}
