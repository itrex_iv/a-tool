import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { delay, map, flatMap } from 'rxjs/operators';
import { DownloadJobMetaCommand, SetJobStatusCommand, SetJobDetectionsQtyCommand } from '../commands/impl';
import { DownloadMetaFailureEvent } from '../events/impl/download-meta/download-meta-failure.event';
import { currentStatus } from '../interfaces';
import { DownloadImageFailureEvent } from '../../images/events/impl/download-image-failure.event';
import { DownloadImageSuccessEvent } from '../../images/events/impl/download-image-success.event';
import { JobCreationInitiationEvent } from '../events/impl/job-creation/job-creation-initiation.event';
import { DownloadMetaSuccessEvent } from '../events/impl/download-meta/download-meta-success.event';

@Injectable()
export class JobsSagas {
  @Saga()
  jobCreationStarted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(JobCreationInitiationEvent),
      delay(1000),
      map(event => {
        return [
          new SetJobStatusCommand(event.job_id, currentStatus.CreationInProcess),
          new DownloadJobMetaCommand(event.annotator_id, event.job_id),
        ];
      }),
      flatMap(c => c),
    );
  };

  @Saga()
  downloadMetaSuccess = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadMetaSuccessEvent),
      delay(1000),
      map(event => {
        return new SetJobDetectionsQtyCommand(event.job_id, event.detections_qty, event.detections_with_track_id, event.camerasFramesCount);
      }),
    );
  };

  @Saga()
  jobCreationSuccess = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadImageSuccessEvent),
      delay(1000),
      map(event => {
        return new SetJobStatusCommand(event.job_id, currentStatus.SucceededInCreation);
      }),
    );
  };

  @Saga()
  downloadMetaFailing = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadMetaFailureEvent),
      delay(1000),
      map(event => {
        return new SetJobStatusCommand(event.job_id, currentStatus.FailedInCreation);
      }),
    );
  };

  @Saga()
  downloadImageFailing = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DownloadImageFailureEvent),
      delay(1000),
      map(event => {
        return new SetJobStatusCommand(event.job_id, currentStatus.FailedInCreation);
      }),
    );
  };
}
