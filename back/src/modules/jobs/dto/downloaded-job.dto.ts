/* tslint:disable:max-classes-per-file*/

import { Expose, Exclude, Transform, Type } from 'class-transformer';
import { Job, CameraInfo } from '../interfaces';
import { ValidateNested } from 'class-validator';
import * as moment from 'moment';

@Exclude()
export class DownloadedJob {
  @Expose({ name: 'timestamp_start' })
  @Transform(val => moment(val).unix())
  public readonly start_date: number;

  @Expose({ name: 'timestamp_end' })
  @Transform(val => moment(val).unix())
  public readonly end_date: number;

  @Expose()
  public readonly session: number;

  @Expose()
  public readonly job_id: string;

  @Expose()
  public readonly job_name: string;

  @Expose()
  public readonly duration: number;

  @Expose()
  public readonly frame_rate: number;

  @Expose()
  @ValidateNested({ each: true })
  @Type(() => CameraInfoDto)
  public readonly camera_info: CameraInfo[];

  public readonly job_progress: number = 0;
}

@Exclude()
class CameraInfoDto {
  @Expose()
  public readonly camera_id: number;
  @Expose()
  public readonly name: string;
  @Expose()
  public readonly image_width: number;
  @Expose()
  public readonly image_height: number;
}
