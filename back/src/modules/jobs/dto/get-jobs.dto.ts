import { IsNumber, Min } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export class GetJobs {
  @ApiModelProperty({ minimum: 0 })
  @IsNumber()
  @Min(0)
  @Transform(value => Number(value))
  public readonly from: number;

  @ApiModelProperty({ minimum: 0 })
  @IsNumber()
  @Min(0)
  @Transform(value => Number(value))
  public readonly to: number;

  @ApiModelProperty({ minimum: 1 })
  @IsNumber()
  @Min(1)
  @Transform(value => Number(value))
  public readonly page: number;

  @ApiModelProperty({ minimum: 1 })
  @IsNumber()
  @Min(1)
  @Transform(value => Number(value))
  public readonly per_page: number;
}
