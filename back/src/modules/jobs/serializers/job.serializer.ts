/* tslint:disable:max-classes-per-file*/

import { Exclude, Expose, Type } from 'class-transformer';
import { CameraInfo } from '../interfaces';
import { ApiModelProperty } from '@nestjs/swagger';
import { ValidateNested } from 'class-validator';

@Exclude()
export class CameraInfoSerializer {
  @ApiModelProperty()
  @Expose()
  camera_id: number;

  @ApiModelProperty()
  @Expose()
  name: string;

  @ApiModelProperty()
  @Expose()
  image_width: number;

  @ApiModelProperty()
  @Expose()
  image_height: number;

  @ApiModelProperty()
  @Expose()
  frames_count: number;
}

@Exclude()
export class JobSerializer {
  @Exclude()
  _id: object;

  @Exclude()
  session: number;

  @ApiModelProperty()
  @Expose()
  job_id: string;

  @ApiModelProperty()
  @Expose()
  job_name: string;

  @ApiModelProperty()
  @Expose()
  duration: number;

  @ApiModelProperty()
  @Expose()
  frame_rate: number;

  @ApiModelProperty()
  @Expose()
  start_date: number;

  @ApiModelProperty()
  @Expose()
  end_date: number;

  @ApiModelProperty()
  @Expose()
  job_progress: number;

  @ApiModelProperty({ type: [CameraInfoSerializer] })
  @ValidateNested({ each: true })
  @Type(() => CameraInfoSerializer)
  @Expose()
  camera_info: CameraInfo[];

  constructor(partial: Partial<JobSerializer>) {
    Object.assign(this, partial);
  }
}
