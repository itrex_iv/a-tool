import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export enum message {
  AnnotatorNotFound = 'Annotator not found!',
}

export class AnnotatorNotFoundOpenAPIResponseType {
  @ApiModelProperty({ example: 'Not Found' })
  error: string;

  @ApiModelProperty({ enum: message })
  message: string;
}

export const AnnotatorNotFoundOpenAPIResponse = {
  status: HttpStatus.NOT_FOUND,
  type: AnnotatorNotFoundOpenAPIResponseType,
};
