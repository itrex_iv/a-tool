import { Module, HttpModule, HttpService } from '@nestjs/common';
import { JobsController } from './jobs.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { DatabaseModule } from '../../core/database/database.module';
import { JobsRepository } from './repository/jobs.repository';
import { jobsProviders } from './providers/jobs.providers';
import { FilestorageModule } from '../../core/filestorage/filestorage.module';
import { commandHandlers } from './commands/handlers';
import { eventHandlers } from './events/handlers';
import { queryHandlers } from './queries/handlers';
import { ConfigService } from '../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../shared/config/constants';
import { JobsSagas } from './sagas/jobs.sagas';

@Module({
  imports: [
    CqrsModule,
    DatabaseModule,
    FilestorageModule,
    HttpModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        baseURL: configService.get(ENV_VARIABLE_KEY.JOB_SERVER_DOMAIN),
        timeout: 10000,
        maxRedirects: 1,
      }),
    }),
  ],
  controllers: [JobsController],
  providers: [JobsRepository, ...jobsProviders, ...commandHandlers, ...eventHandlers, ...queryHandlers, JobsSagas],
  exports: [JobsRepository],
})
export class JobsModule {}
