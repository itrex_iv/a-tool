import { Controller, Get, Param, Query, UseGuards, ClassSerializerInterceptor, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetJobs } from './dto';
import { Roles } from '../../core/auth/decorators/roles.decorator';
import { RolesGuard } from '../../core/auth/guards/role.guard';
import { JobSerializer } from './serializers/job.serializer';
import { GetJobsQuery } from './queries/impl';
import { Job } from './interfaces';
import { QueryBus } from '@nestjs/cqrs';
import { ApiUseTags, ApiBearerAuth, ApiOkResponse, ApiResponse } from '@nestjs/swagger';
import { AnnotatorNotFoundOpenAPIResponse } from './openapi/annotator-not-found';
import { InternalServerErrorOpenAPIResponse } from '../../shared/openapi/internal-server-error';
import { UserUnauthorizedErrorOpenAPIResponse } from '../../shared/openapi/user-unauthorized-error';
import { BadRequestErrorOpenAPIResponse } from '../../shared/openapi/bad-request-error';

@ApiBearerAuth()
@ApiUseTags('Job')
@Controller('annotators')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class JobsController {
  constructor(private readonly queryBus: QueryBus) {}

  @ApiOkResponse({ type: [JobSerializer] })
  @ApiResponse(AnnotatorNotFoundOpenAPIResponse)
  @ApiResponse(BadRequestErrorOpenAPIResponse)
  @ApiResponse(UserUnauthorizedErrorOpenAPIResponse)
  @ApiResponse(InternalServerErrorOpenAPIResponse)
  @Roles('annotator')
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':annotator_id/jobs/query/getJobs')
  async getJobs(@Param('annotator_id') annotator_id: string, @Query() queryDto: GetJobs) {
    const jobs = await this.queryBus.execute<GetJobsQuery, Job[]>(
      new GetJobsQuery(annotator_id, queryDto.from, queryDto.to, queryDto.page, queryDto.per_page),
    );
    return jobs.map(job => new JobSerializer(job));
  }
}
