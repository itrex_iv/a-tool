import { currentStatus } from '../../interfaces';

export class SetJobStatusCommand {
  constructor(public readonly job_id: string, public readonly currentStatus: currentStatus) {}
}
