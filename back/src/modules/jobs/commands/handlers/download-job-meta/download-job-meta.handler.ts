import { fork } from 'child_process';
import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { DownloadJobMetaCommand } from '../../impl';
import { ConfigService } from '../../../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../../../shared/config/constants';
import { DownloadMetaSuccessEvent } from '../../../events/impl/download-meta/download-meta-success.event';
import { DownloadMetaFailureEvent } from '../../../events/impl/download-meta/download-meta-failure.event';
import { JobsRepository } from '../../../repository/jobs.repository';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(DownloadJobMetaCommand)
export class DownloadJobMetaHandler implements ICommandHandler<DownloadJobMetaCommand> {
  constructor(
    private readonly configService: ConfigService,
    private readonly eventBus: EventBus,
    private readonly jobsRepository: JobsRepository,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ annotator_id, job_id }: DownloadJobMetaCommand) {
    this.appLogger.command(`DownloadJobMeta`);
    try {
      const { frame_rate } = await this.jobsRepository.getJobById(job_id);
      const forked = fork(`${__dirname}/download-job-meta.fork.js`);
      forked.send({
        job_id,
        frame_rate,
        baseURL: this.configService.get(ENV_VARIABLE_KEY.JOB_SERVER_DOMAIN),
        baseDir: this.configService.get(ENV_VARIABLE_KEY.FS_ROOT_DIR),
      });

      forked.on('message', msg => {
        if (msg.error) {
          console.log(msg);
          this.eventBus.publish(new DownloadMetaFailureEvent(annotator_id, job_id));
        } else {
          this.eventBus.publish(
            new DownloadMetaSuccessEvent(annotator_id, job_id, msg.detections_qty, msg.detections_with_track_id, msg.camerasFramesCount),
          );
        }
      });
    } catch (e) {
      console.log(e);
      this.eventBus.publish(new DownloadMetaFailureEvent(annotator_id, job_id));
    }
  }
}
