import * as fs from 'fs-extra';
import axios from 'axios';

process.on('message', async ({ baseURL, job_id, frame_rate, baseDir }) => {
  try {
    const { data } = await axios.get(`jobs/${job_id}/meta`, { baseURL });
    await fs.ensureDir(`${baseDir}/jobs/${job_id}`);
    await fs.ensureDir(`${baseDir}/jobs/${job_id}/raw`);
    await fs.ensureDir(`${baseDir}/jobs/${job_id}/sessions`);
    let detections_qty = 0;
    let detections_with_track_id = 0;
    const camerasFramesCount = {};
    for (const index in data[0].multi_view) {
      // tslint:disable-line
      if (!data[0].multi_view[index].camera_frames) continue;
      for (const frame of data[0].multi_view[index].camera_frames) {
        await fs.ensureDir(`${baseDir}/jobs/${job_id}/raw/${frame.camera_id}/${index}/`);
        frame.frameTime = frame_rate * parseInt(index, 10);
        frame.timestamp = data[0].multi_view[index].timestamp;
        frame.detections = frame.detections || [];

        detections_qty += frame.detections.length;
        detections_with_track_id = frame.detections.reduce((a, detection) => a + (detection.track_id === -1 ? 0 : 1), detections_with_track_id);
        await fs.writeJson(`${baseDir}/jobs/${job_id}/raw/${frame.camera_id}/${index}/frame.json`, frame);

        camerasFramesCount[frame.camera_id] = camerasFramesCount[frame.camera_id] ? camerasFramesCount[frame.camera_id] + 1 : 1;
      }
    }
    process.send({ detections_qty, detections_with_track_id, camerasFramesCount, error: false });
  } catch (e) {
    process.send({ error: true, msg: e });
  }
});
