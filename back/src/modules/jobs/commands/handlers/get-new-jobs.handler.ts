import { CommandHandler, ICommandHandler, QueryBus } from '@nestjs/cqrs';
import { GetNewJobsCommand } from '../impl';
import { DownloadJobsListQuery } from '../../queries/impl';
import { JobsRepository } from '../../repository/jobs.repository';
import { Job } from '../../interfaces';
import { AppLogger } from '../../../../shared/logger/logger.service';

@CommandHandler(GetNewJobsCommand)
export class GetNewJobsHandler implements ICommandHandler<GetNewJobsCommand> {
  constructor(private readonly queryBus: QueryBus, public readonly jobsRepository: JobsRepository, public readonly appLogger: AppLogger) {}

  async execute({  }: GetNewJobsCommand) {
    this.appLogger.command(`GetNewJobs`);
    const allJobs: Job[] = await this.queryBus.execute(new DownloadJobsListQuery());
    const allJobsIds = (await this.jobsRepository.getAllJobsIds()).map(job => job.job_id);
    const newJobs = allJobs.filter(({ job_id }) => !allJobsIds.includes(job_id));
    for (const job of newJobs) {
      await this.jobsRepository.create(job);
    }
  }
}
