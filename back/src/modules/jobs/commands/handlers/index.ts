import { CopySessionHandler } from './copy-session/copy-session.handler';
import { DownloadJobMetaHandler } from './download-job-meta/download-job-meta.handler';
import { GetNewJobsHandler } from './get-new-jobs.handler';
import { SetJobStatusHandler } from './set-job-status.handler';
import { SetJobDetectionQtyHandler } from './set-job-detections_qty.handler';

export const commandHandlers = [CopySessionHandler, DownloadJobMetaHandler, GetNewJobsHandler, SetJobStatusHandler, SetJobDetectionQtyHandler];
