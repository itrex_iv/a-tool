import * as fs from 'fs-extra';

process.on('message', async ({ annotator_id, job_id, jobsDir }) => {
  try {
    const pathToRaw = `${jobsDir}/${job_id}/raw`;
    const pathToSession = `${jobsDir}/${job_id}/sessions/${annotator_id}`;
    await fs.copy(pathToRaw, pathToSession);

    process.send({ error: false });
  } catch (e) {
    process.send({ error: true, msg: e });
  }
});
