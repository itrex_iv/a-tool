import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { CopySessionCommand } from '../../impl';
import { FilestorageService } from '../../../../../core/filestorage/filestorage.service';
import { AnnotationSessionCreationSuccessEvent } from '../../../events/impl/annotation-session-creation/annotation-session-creation-success.event';
import { AnnotationSessionCreationFailureEvent } from '../../../events/impl/annotation-session-creation/annotation-session-creation-failure.event';
import { fork } from 'child_process';
import { JobsRepository } from '../../../repository/jobs.repository';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@CommandHandler(CopySessionCommand)
export class CopySessionHandler implements ICommandHandler<CopySessionCommand> {
  constructor(
    public readonly filestorageService: FilestorageService,
    private readonly eventBus: EventBus,
    private readonly jobsRepository: JobsRepository,
    private readonly appLogger: AppLogger,
  ) {}

  async execute({ annotator_id, job_id }: CopySessionCommand) {
    this.appLogger.command(`CopySession`);
    try {
      const forked = fork(`${__dirname}/copy-session.fork.js`);
      forked.send({ annotator_id, job_id, jobsDir: this.jobsRepository.jobsDir });

      forked.on('message', msg => {
        if (msg.error) {
          console.log(msg);
          this.eventBus.publish(new AnnotationSessionCreationFailureEvent(annotator_id, job_id));
        } else this.eventBus.publish(new AnnotationSessionCreationSuccessEvent(annotator_id, job_id));
      });
    } catch (e) {
      console.log(e);
      this.eventBus.publish(new AnnotationSessionCreationFailureEvent(annotator_id, job_id));
    }
  }
}
