import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SetJobStatusCommand } from '../impl';
import { JobsRepository } from '../../repository/jobs.repository';
import { AppLogger } from '../../../../shared/logger/logger.service';

@CommandHandler(SetJobStatusCommand)
export class SetJobStatusHandler implements ICommandHandler<SetJobStatusCommand> {
  constructor(public readonly jobsRepository: JobsRepository, public readonly appLogger: AppLogger) {}

  async execute({ job_id, currentStatus }: SetJobStatusCommand) {
    this.appLogger.command(`SetJobStatus: ${currentStatus}`);
    const job = await this.jobsRepository.getJobById(job_id);
    job.currentStatus = currentStatus;
    await this.jobsRepository.save(job);
  }
}
