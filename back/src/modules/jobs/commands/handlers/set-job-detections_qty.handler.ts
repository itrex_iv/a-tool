import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SetJobDetectionsQtyCommand } from '../impl';
import { JobsRepository } from '../../repository/jobs.repository';
import { AppLogger } from '../../../../shared/logger/logger.service';

@CommandHandler(SetJobDetectionsQtyCommand)
export class SetJobDetectionQtyHandler implements ICommandHandler<SetJobDetectionsQtyCommand> {
  constructor(public readonly jobsRepository: JobsRepository, private readonly appLogger: AppLogger) {}

  async execute({ job_id, detections_qty, detections_with_track_id, camerasFramesCount }: SetJobDetectionsQtyCommand) {
    this.appLogger.command(`SetJobDetectionsQty: ${detections_qty} ${detections_with_track_id}`);
    const job = await this.jobsRepository.getJobById(job_id);
    job.camera_info = job.camera_info.map(camera_info => {
      return {
        frames_count: camerasFramesCount[camera_info.camera_id],
        ...camera_info,
      };
    });
    job.detections_qty = detections_qty;
    job.detections_with_track_id = detections_with_track_id;
    await this.jobsRepository.save(job);
  }
}
