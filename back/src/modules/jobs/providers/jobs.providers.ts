import { Connection } from 'mongoose';
import { jobSchema } from '../repository/schemas/job.schema';

export const jobsProviders = [
  {
    provide: 'JOB_MODEL',
    useFactory: (connection: Connection) => connection.model('jobs', jobSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
