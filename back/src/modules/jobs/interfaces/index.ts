export enum currentStatus {
  CreationInProcess = 'CreationInProcess',
  FailedInCreation = 'FailedInCreation',
  SucceededInCreation = 'SucceededInCreation',
}

export interface Job {
  session: number;
  job_id: string;
  job_name: string;
  duration: number;
  frame_rate: number;
  start_date?: number;
  end_date?: number;
  camera_info: CameraInfo[];
  detections_qty?: number;
  detections_with_track_id?: number;
  job_progress?: number;
  currentStatus: currentStatus;
}

export interface CameraInfo {
  camera_id: number;
  name: string;
  image_width: number;
  image_height: number;
  frames_count?: number;
}

export interface MongooseJob extends Job, Document {}
