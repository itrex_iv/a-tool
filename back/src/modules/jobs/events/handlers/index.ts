import { AnnotationSessionCreationFailureHandler } from './annotation-session-creation/annotation-session-creation-failure.handler';
import { AnnotationSessionCreationSuccessHandler } from './annotation-session-creation/annotation-session-creation-success.handler';
import { JobCreationFailureHandler } from './job-creation/job-creation-failure.handler';
import { JobCreationSuccessHandler } from './job-creation/job-creation-success.handler';
import { DownloadMetaFailureHandler } from './download-meta/download-meta-failure.handler';
import { DownloadMetaSuccessHandler } from './download-meta/download-meta-success.handler';
import { JobCreationInitiationHandler } from './job-creation/job-creation-initiation.handler';

export const eventHandlers = [
  AnnotationSessionCreationFailureHandler,
  AnnotationSessionCreationSuccessHandler,
  JobCreationFailureHandler,
  JobCreationSuccessHandler,
  DownloadMetaFailureHandler,
  DownloadMetaSuccessHandler,
  JobCreationInitiationHandler,
];
