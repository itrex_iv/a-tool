import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { DownloadMetaSuccessEvent } from '../../impl/download-meta/download-meta-success.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(DownloadMetaSuccessEvent)
export class DownloadMetaSuccessHandler implements IEventHandler<DownloadMetaSuccessEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: DownloadMetaSuccessEvent) {
    this.appLogger.event(`DownloadMetaSuccess`);
    return;
  }
}
