import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { DownloadMetaFailureEvent } from '../../impl/download-meta/download-meta-failure.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(DownloadMetaFailureEvent)
export class DownloadMetaFailureHandler implements IEventHandler<DownloadMetaFailureEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: DownloadMetaFailureEvent) {
    this.appLogger.event(`DownloadMetaFailure`);
    return;
  }
}
