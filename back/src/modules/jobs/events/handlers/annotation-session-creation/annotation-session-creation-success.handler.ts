import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { AnnotationSessionCreationSuccessEvent } from '../../impl/annotation-session-creation/annotation-session-creation-success.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(AnnotationSessionCreationSuccessEvent)
export class AnnotationSessionCreationSuccessHandler implements IEventHandler<AnnotationSessionCreationSuccessEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: AnnotationSessionCreationSuccessEvent) {
    this.appLogger.event(`AnnotationSessionCreationSuccess`);
    return;
  }
}
