import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { AnnotationSessionCreationFailureEvent } from '../../impl/annotation-session-creation/annotation-session-creation-failure.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(AnnotationSessionCreationFailureEvent)
export class AnnotationSessionCreationFailureHandler implements IEventHandler<AnnotationSessionCreationFailureEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: AnnotationSessionCreationFailureEvent) {
    this.appLogger.event(`AnnotationSessionCreationFailure`);
    return;
  }
}
