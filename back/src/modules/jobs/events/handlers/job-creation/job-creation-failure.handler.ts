import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { JobCreationFailureEvent } from '../../impl/job-creation/job-creation-failure.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(JobCreationFailureEvent)
export class JobCreationFailureHandler implements IEventHandler<JobCreationFailureEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: JobCreationFailureEvent) {
    this.appLogger.event(`JobCreationFailure`);
    return;
  }
}
