import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { JobCreationSuccessEvent } from '../../impl/job-creation/job-creation-success.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(JobCreationSuccessEvent)
export class JobCreationSuccessHandler implements IEventHandler<JobCreationSuccessEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: JobCreationSuccessEvent) {
    this.appLogger.event(`JobCreationSuccess`);
    return;
  }
}
