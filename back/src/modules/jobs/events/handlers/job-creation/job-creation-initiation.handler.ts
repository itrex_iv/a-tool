import { IEventHandler } from '@nestjs/cqrs';
import { EventsHandler } from '@nestjs/cqrs/dist/decorators/events-handler.decorator';
import { JobCreationInitiationEvent } from '../../impl/job-creation/job-creation-initiation.event';
import { AppLogger } from '../../../../../shared/logger/logger.service';

@EventsHandler(JobCreationInitiationEvent)
export class JobCreationInitiationHandler implements IEventHandler<JobCreationInitiationEvent> {
  constructor(private readonly appLogger: AppLogger) {}

  handle(event: JobCreationInitiationEvent) {
    this.appLogger.event(`JobCreationInitiation`);
    return;
  }
}
