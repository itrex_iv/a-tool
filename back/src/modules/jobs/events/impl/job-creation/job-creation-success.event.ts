export class JobCreationSuccessEvent {
  constructor(public readonly job_id: string) {}
}
