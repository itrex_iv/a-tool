export class JobCreationFailureEvent {
  constructor(public readonly job_id: string) {}
}
