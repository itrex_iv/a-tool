export class DownloadMetaSuccessEvent {
  constructor(
    public readonly annotator_id: string,
    public readonly job_id: string,
    public readonly detections_qty: number,
    public readonly detections_with_track_id: number,
    public readonly camerasFramesCount: object,
  ) {}
}
