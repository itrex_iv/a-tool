import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export class UserUnauthorizedErrorOpenAPIResponseType {
  @ApiModelProperty({ example: 'Unauthorized' })
  error: string;
}

export const UserUnauthorizedErrorOpenAPIResponse = {
  status: HttpStatus.UNAUTHORIZED,
  type: UserUnauthorizedErrorOpenAPIResponseType,
};
