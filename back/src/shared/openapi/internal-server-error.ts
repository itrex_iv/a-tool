import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export class InternalServerErrorOpenAPIResponseType {
  @ApiModelProperty({ example: 'Internal server error' })
  message: string;
}

export const InternalServerErrorOpenAPIResponse = {
  status: HttpStatus.INTERNAL_SERVER_ERROR,
  type: InternalServerErrorOpenAPIResponseType,
};
