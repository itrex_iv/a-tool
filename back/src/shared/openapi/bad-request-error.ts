import { ApiModelProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export class BadRequestErrorOpenAPIResponseType {
  @ApiModelProperty({ example: 'Bad Request' })
  error: string;

  @ApiModelProperty()
  message: object;
}

export const BadRequestErrorOpenAPIResponse = {
  status: HttpStatus.BAD_REQUEST,
  type: BadRequestErrorOpenAPIResponseType,
};
