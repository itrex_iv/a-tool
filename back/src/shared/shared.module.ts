import { ConfigModule } from './config/config.module';
import { LoggerModule } from './logger/logger.module';

export const sharedModules = [ConfigModule, LoggerModule];
