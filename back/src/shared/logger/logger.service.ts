import * as clc from 'cli-color';
import { Logger } from '@nestjs/common';

export class AppLogger extends Logger {
  command(message: string) {
    console.log(clc.blue(`>Command: ${message}`));
  }
  query(message: string) {
    console.log(clc.white(`>>Query: ${message}`));
  }
  event(message: string) {
    console.log(clc.magenta(`:Event: ${message}`));
  }
}
