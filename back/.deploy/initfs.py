import os

mainFolder = '../a-tool-fs'
subFolders = ['images', 'jobs']

def main():
    for subFolder in subFolders:
        dir = '%s/%s' % (mainFolder, subFolder)
        if not os.path.exists(dir):
            os.makedirs(dir)

if __name__ == "__main__":
    main()