const fs = require('fs-extra');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

(async () => {
  const env = dotenv.parse(fs.readFileSync(`${process.env.NODE_ENV}.env`));
  mongoose.connect(env.DB_HOST, { useNewUrlParser: true });

  // Remove all sessions
  const Annotator = mongoose.model(
    'annotator',
    mongoose.Schema({
      active_sessions: [],
    }),
  );
  try {
    const annotator = await Annotator.findById('5d6908fe597e5d11ae29c259');
    annotator.active_sessions = [];
    await annotator.save();

    // Remove all jobs
    await mongoose.connection.db.dropCollection('jobs');

    //Remove all data from fs
    await fs.emptyDir(`${env.FS_ROOT_DIR}/jobs/`);
    await fs.emptyDir(`${env.FS_ROOT_DIR}/images/`);
  } catch (e) { }

  mongoose.connection.close();
})();
