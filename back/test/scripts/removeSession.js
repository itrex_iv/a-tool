const fs = require('fs-extra');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

(async () => {
  if (process.argv.length != 3) {
    throw new Error('You should pass job_id as the first argument!');
  }
  const job_id = process.argv[2];

  const env = dotenv.parse(fs.readFileSync(`${process.env.NODE_ENV}.env`));
  mongoose.connect(env.DB_HOST, { useNewUrlParser: true });

  // Remove job from session
  const Annotator = mongoose.model(
    'annotator',
    mongoose.Schema({
      active_sessions: [],
    }),
  );
  const annotator = await Annotator.findById('5d6908fe597e5d11ae29c259');
  annotator.active_sessions = annotator.active_sessions.filter(ses => ses.job_id !== job_id);
  await annotator.save();

  // Remove status from Job
  const Job = mongoose.model('job', {
    job_id: String,
    detections_qty: Number,
    detections_with_track_id: Number,
    currentStatus: String,
  });
  const job = await Job.findOne({ job_id });
  job.currentStatus = undefined;
  job.detections_qty = undefined;
  job.detections_with_track_id = undefined;
  await job.save();

  //Remove job data from fs
  await fs.remove(`${env.FS_ROOT_DIR}/jobs/${job_id}`);
  await fs.remove(`${env.FS_ROOT_DIR}/images/${job_id}`);
  mongoose.connection.close();
})();
